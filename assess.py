from blessings import Terminal

cell_size = 3

def get_grid_data(data):
    coordinates = {}
    t = Terminal()
    for entry in data:
        position = entry.get_position()
        newkey = f'{position[0]},{position[1]}'
        shortname = entry.classname_short
        coordinates[newkey] = t.underline(f"{shortname:<{cell_size}}")
    return coordinates


def show_it(data):
    grid_contents = get_grid_data(data)
    t = Terminal()

    underline = t.underline(' ')
    max_y = 10

    grids = [[f'{underline*cell_size}' for _ in range(max_y)] for _ in range(max_y)]
    # y-coords
    for y, column in enumerate(grids):
        # x-coords
        for x, row in enumerate(column):
            thekey = f'{x},{y}'
            try:
                letter = grid_contents[thekey]
            except KeyError:
                continue
            if len(letter) < cell_size:
                print('it is less')
                letter = f'{letter}{underline*(cell_size-len(letter))}'
            column[x] = letter

    top_row = ''
    # draw the top
    for _ in range((cell_size + 1) * max_y):
        top_row = top_row + underline
    print(top_row)
    for row in grids:
        print('|'.join(row) + "|")
