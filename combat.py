from blessings import Terminal
from examples import custom_style_3
from PyInquirer import prompt
import random
import time

from assess import show_it
from entities import party, generate_members
from load_data import class_abilities, monster_classes
from settings import config

"""
# combat

each entity would have an xy coordinate
distance calculated by number of squares
each entity would take a turn
options would be [attack,[abilities],assess,end]

features:
    combat:
        assess:
            would give option to assess a target or show current state of battlefield in grid form
            if target, list of targets is provided
        abilities:
            fireball, warcry, gamble, etc
            requires target selection
            types:
                - health-based
                    offensive and recovery
                - condition
                    applies a buff or debuff
bugs:
future enhancements:
    add target matching on attack
    add hp manipulation to attacks
    enable abilities (basically just attacks with other attributes)
    add conditions (buffs/debuffs)
snags:
"""


def attack(roster, entity):
    if entity.is_player_character:
        # show enemy targets
        summarized_roster = list(filter(lambda x:
            not x.is_player_character and \
            x.is_in_range(entity, entity.attack_reach)
            , roster
        ))
    else:
        # pick player target at random/in range
        # show player targets
        summarized_roster = list(filter(lambda x:
            # only attack opposite entity type
            x.is_player_character and \
            # override attack reach with entry's attack_reach
            x.is_in_range(entity, config.enemy_attack_reach)
            , roster
        ))

    # compare each entry in the roster to current entity
    summarized_roster = list(map(lambda x: f'{x.assess(entity)}', summarized_roster))
    if not summarized_roster:
        print('no targets in range')
        return

    # offer the choice if player
    if entity.is_player_character:
    #if True: #Always true for testing
        questions = [{
            'type': 'list',
            'name': 'attack_target',
            'message': 'attack which target?',
            'choices': summarized_roster,
        }]
        answers = prompt(questions)
        target = answers.get('attack_target')
        print(f'would attack: {target}')
        return True
    else:
        target = random.choice(summarized_roster)
        # get random player target from roster
        print('attacking target', target)

    # how to get actual object from string? include key in string
    # since no 2 entities can share a space, can just use position
    # pattern match: position (x, y)
    # target.process_attack(entity.attack_power)


def assess(roster, entity):
    # assess should give option to assess a target or show current state of battlefield in grid form
        # if target, list of targets is provided
    if not entity.is_player_character:
        return

    assess_response = get_assess_choice(roster)
    if assess_response == 'field':
        show_it(roster)
    else:
        targets = list(filter(lambda x: x.classname_short == assess_response, roster))
        target_reprs = list(map(lambda x: x.assess(entity), targets))
        for entry in target_reprs:
            print()
            print(entry)


def end(roster, entity):
    # perform turn end operations here
    pass


def execute_ability(roster, entity):
    print('would execute ability')


def move(roster, entity):
    show_it(roster)
    used = False

    if entity.is_player_character:
        movement = ()
        while not movement:
            movement = get_player_movement(entity)

        if movement[0] or movement[1]:
            print(f'{entity.name} the {entity.classname} has moved (x, y): {movement}')
            entity.position['x'] += movement[0]
            entity.position['y'] += movement[1]
            used = True
        else:
            print('\nnot moving, valid input not received')
    else:
        # it's a monster, so randomize
        # get class movement availability
        to_move = random.randrange(0, entity.movement)
        print('to move is', to_move)
        # track total move this round
        total_move = 0

        # calculate available movement
        available = to_move - total_move
        print('available is', available)

        # get movement in x direction
        if available:
            x_diff = random.randrange(-available, available)
            total_move += abs(x_diff)
            entity.position['x'] += x_diff
            used = True

        # calculate available movement
        available = to_move - total_move
        print('available is', available)

        # get movement in y direction
        if available:
            y_diff = random.randrange(-available, available)
            entity.position['y'] += y_diff
            used = True

    show_it(roster)
    return used


def convert_to_integer(text):
    """
    using a function instead of a lambda here, because negatives are hard to deal with
    `isdigit` works with lstrip of the '-', but '-----' will pass as well, which
    will then fail to convert to an integer
    """
    try:
        converted = int(text)
        return converted
    except ValueError:
        return 0


def test_move_entry(text, entity_data):
    converted = convert_to_integer(text)

    if abs(converted) <= entity_data.remaining_move:
        entity_data.remaining_move -= abs(converted)
        return True
    else:
        return False


def get_movement_message(player_data):
    return f'movement in y? (total allowed: {player_data.remaining_move})'


# can use classes as validators too
# https://github.com/CITGuru/PyInquirer/blob/master/examples/pizza.py
def get_player_movement(player_data):
    allowed_move = player_data.movement
    player_data.remaining_move = player_data.movement
    print()
    questions = [
        {
            'type'    : 'input',
            'name'    : 'x_movement',
            'message' : f'movement in x? (total allowed: {player_data.remaining_move})',
            'validate': lambda text: test_move_entry(text, player_data) or 'greater than allowed',
            'default' : "",
        },
        {
            'type'    : 'input',
            'name'    : 'y_movement',
            'message' : get_movement_message(player_data),
            #'message' : f'movement in y? (total allowed: {player_data.remaining_move})',
            'validate': lambda text: test_move_entry(text, player_data) or 'greater than allowed',
            'default' : "",
        },
    ]
    answers = prompt(questions)
    if not answers:
        return ()
    x_move = convert_to_integer(answers.get('x_movement'))
    y_move = convert_to_integer(answers.get('y_movement'))
    return (x_move, y_move)


common_commands = {
    'attack': attack,
    'assess': assess,
    'end': end,
    'move': move,
}


def update_commands(entity):
    entity_abils = entity.abilities
    ability_commands = {}
    for key in [*entity_abils.keys()]:
        if entity_abils[key]['type'] == 'action':
            ability_commands[key] = execute_ability
    return {**ability_commands, **common_commands}


def get_nonplayer_choice(entity, commands):
    name = entity.name
    entity_class = entity.classname
    command = random.choice(list(commands.keys()))
    print(f'{name} the {entity_class} is {command}ing ahhh!')
    #time.sleep(3)
    return command


def choose_action(roster, commands, current_entity):
    command = None
    print()
    response_var = 'selected_command'

    while not command:
        if current_entity.is_player_character:
            questions = [{
                'type': 'list',
                'name': response_var,
                'message': f"{current_entity.name}'s ({current_entity.classname_short}) command",
                'choices': commands
            }]
            answers = prompt(questions, style=custom_style_3)
            command_name = answers[response_var]
        else:
            command_name = get_nonplayer_choice(current_entity, commands)

        command = commands[command_name]

    result = command(roster, current_entity)

    delete_command(commands, command_name, result)

    return command_name == 'end'


def delete_command(commands, command_name, result):
    """
    remove command from available commands
    """
    if command_name == 'move' and not result:
        return
    elif command_name == 'assess':
        return
    elif command_name == 'attack' and not result:
        return

    print('deleting command')
    del commands[command_name]


def get_assess_choice(roster):
    questions = [{
        'type': 'list',
        'name': 'target_type',
        'message': 'assess what?',
        'choices': ['field', 'targets'],
    }]
    answers = prompt(questions)
    target_type = answers.get('target_type')
    if target_type == 'field':
        return target_type
    if target_type == 'targets':
        summarized_roster = list(map(lambda x: f'{x.classname_short}', roster))
        questions = [{
            'type': 'list',
            'name': 'assess_target',
            'message': 'assess which target?',
            'choices': summarized_roster,
        }]
        answers = prompt(questions)
        return answers.get('assess_target')


def generate_entities(party_members):
    print('generating enemies')
    generated = []
    monsters = generate_members(['monster'] * 3, generated)
    return party_members + generated


def set_player_positions():
    print('setting player positions')


def run_combat():
    roster = generate_entities(party)
    set_player_positions()
    in_combat = True
    rounds = 0
    while in_combat:
        for member in roster:
            commands = update_commands(member)
            process_next = False
            print(f'\n==========================\nnew turn: {member.name} ({member.classname})')
            while not process_next:
                process_next = choose_action(roster, commands, member)
        rounds = rounds + 1
        if rounds > 3:
            print('\ncombat concluded!\n')
            in_combat = False

