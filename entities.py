from examples import custom_style_2
from load_data import class_abilities, class_config, class_stats, player_classes
from PyInquirer import prompt
import random


class GameEntity(object):
    def __init__(self, d):
        # this isn't strictly necessary because `self.__dict__ = d` would do it automatically
        # but without these, it's harder to see what the fields are for reference
        self.abilities           = d.get('abilities')
        self.attack_reach        = d.get('attack_reach')
        self.classname           = d.get('classname')
        self.classname_short     = d.get('classname_short')
        self.hitpoints           = d.get('hitpoints')
        self.is_player_character = d.get('is_player_character')
        self.movement            = d.get('movement')
        self.name                = d.get('name')
        self.position            = d.get('position')
        self.stats               = d.get('stats')
    #def get_attack_repr(self):
    def assess(self, other_entity):
        assess_string = \
            f'{self.get_detailed_name()}\n\
                HP: {self.hitpoints}\n\
                distance: {self.get_distance_repr(other_entity)}; absolute: {self.get_position_repr()}\n\
            '
        return assess_string
    def get_detailed_name(self):
        detailed_name_string = f'{self.classname_short}: {self.name} ({self.classname})'
        return detailed_name_string
    def get_distance_repr(self, other_entity):
        dist_x, dist_y, effective = self.get_distance(other_entity)
        x_str            = f'x: {dist_x}'
        y_str            = f'y: {dist_y}'
        effective_str    = f'effective: {effective}'
        return_str       = f'{x_str}, {y_str}, {effective_str}'
        return return_str
    def get_distance(self, other_entity):
        my_x, my_y       = self.get_position()
        other_x, other_y = other_entity.get_position()
        dist_x           = my_x - other_x
        dist_y           = my_y - other_y
        # can change this later to handle diagonal distance
        effective        = abs(dist_x) + abs(dist_y)
        return dist_x, dist_y, effective
    def get_position_repr(self):
        position = self.get_position()
        return_str = f'{position}'
        return return_str
    def get_position(self):
        my_x = self.position.get('x')
        my_y = self.position.get('y')
        return my_x, my_y
    def is_in_range(self, other_entity, reach):
        _, _, distance = self.get_distance(other_entity)
        return distance <= reach
    def __str__(self):
        # this is for direct prints
        return f'{self.__dict__}'
    def __repr__(self):
        # this is for printing a collection containing the object
        return f'{self.__dict__}'


# TODO: move this to settings?
player_names = ['Gabiona', 'Nuamas', 'Silverthomas', 'Fylerias']
party_size = 3
stats = ['str', 'con', 'int', 'mnd', 'lck']

party_classes = []
party = []


def get_random_name():
    return player_names[len(party_classes)]


def set_up_party():
    confirmed = False
    while not confirmed:
        form_party()
        confirmed = get_confirmation()
    generate_members(party_classes, party)


def form_party():
    while len(party_classes) < party_size:
        print(f'choose {party_size - len(party_classes)} more party members\n')
        get_user_selection()
    print(f'party is {party_classes}\n')


def get_user_selection():
    out_var = 'selected_class'
    options = [x for x in player_classes if x not in party_classes]
    questions = [{
        'type': 'list',
        'name': out_var,
        'message': 'choose a class',
        'choices': options,
    }]
    answers = prompt(questions, style=custom_style_2)

    if not answers:
        return

    print(f'you chose: {answers[out_var]}')
    party_classes.append(answers[out_var])


def get_confirmation():
    questions = [{
        'type': 'confirm',
        'name': 'confirmation',
        'message': 'is this ok?',
        # this will be False later, True for testing
        'default': True,
    }]

    answers = prompt(questions)

    if not answers:
        return False

    if answers.get('confirmation'):
        return True
    else:
        party_classes.clear()
        return False


def generate_members(entities, collection):
    for entry in entities:
        the_entity = set_stats(entry)
        if entry == 'monster':
            # use len of collection to set name, 000, 111, 222, 333, etc
            the_entity.classname_short = f'{len(collection)}' * 3

        collection.append(the_entity)


def set_stats(classname):
    is_player_character = getattr(class_config, classname).get('player')

    # determine position
    x = getattr(class_config, classname).get('start_x')
    y = getattr(class_config, classname).get('start_y')
    if not is_player_character:
        x = random.randrange(6, 10)
        y = random.randrange(6, 10)

    the_entity = {
        'abilities'          : getattr(class_abilities, classname),
        'attack_reach'       : getattr(class_config, classname).get('attack_reach'),
        'classname'          : classname,
        'classname_short'    : getattr(class_config, classname).get('shortname'),
        'hitpoints'          : getattr(class_config, classname).get('hitpoints'),
        'is_player_character': is_player_character,
        'max_hitpoints'      : getattr(class_config, classname).get('hitpoints'),
        'movement'           : getattr(class_config, classname).get('movement'),
        'name'               : player_names[len(party)],
        'position'           : {
                                   'x': x,
                                   'y': y,
                               },
        'stats'              : class_stats,
    }

    as_object = GameEntity(the_entity)

    return as_object
