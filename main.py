from combat import run_combat
from camp import manage_camp
from new_game import start_game

start_game()

while True:
    run_combat()
    manage_camp()
