import json
import os
import yaml
from yaml import Loader

classes_dir = 'data/classes'
classes = [ name for name in os.listdir(classes_dir) if os.path.isdir(os.path.join(classes_dir, name)) ]
player_classes      = []
monster_classes     = []
ability_files       = {}
loaded_abilities    = {}
loaded_class_config = {}
loaded_class_stats  = {}

base_config         = {}
base_stats          = {}
classname_mappings  = {}

# load these and apply missing keys
base_config_file         = f'{classes_dir}/base_config.yml'
base_stats_file          = f'{classes_dir}/base_stats.yml'
classname_mappings_file  = f'{classes_dir}/classname_mappings.yml'

with open(base_config_file) as file:
    base_config = yaml.load(file.read(), Loader=Loader)

with open(base_stats_file) as file:
    base_stats  = yaml.load(file.read(), Loader=Loader)

with open(classname_mappings_file) as file:
    classname_mappings = yaml.load(file.read(), Loader=Loader)

for cur_class in classes:
    # load abilities
    abilities_dir = f'{classes_dir}/{cur_class}/abilities'
    ability_files[cur_class] = [] if not os.path.exists(abilities_dir) else os.listdir(abilities_dir)

    # load class configs
    config_file = f'{classes_dir}/{cur_class}/config.yml'
    if os.path.exists(config_file):
        with open(config_file) as file:
            loaded_class_config_data = yaml.load(file.read(), Loader=Loader)
        class_config_data = {**base_config, **loaded_class_config_data}
    else:
        class_config_data = base_config

    # apply aggregate mappings
    class_config_data['shortname'] = classname_mappings.get(cur_class)

    # set loaded config data entry for current class
    loaded_class_config[cur_class] = class_config_data

    # load class stats
    stats_file = f'{classes_dir}/{cur_class}/stats.yml'
    if os.path.exists(stats_file):
        with open(stats_file) as file:
            loaded_class_stats_data = yaml.load(file.read(), Loader=Loader)
        class_stats_data = {**base_stats, **loaded_class_stats_data}
    else:
        class_status_data = base_stats
    # set loaded class data entry for current class
    loaded_class_stats[cur_class] = class_stats_data

    # find player classes
    # if no class config, assume player
    if cur_class not in loaded_class_config:
        player_classes.append(cur_class)
    elif 'player' in loaded_class_config.get(cur_class) and loaded_class_config.get(cur_class).get('player'):
        player_classes.append(cur_class)
    else:
        monster_classes.append(cur_class)

    #print(f'player classes are {player_classes}')
    #print(f'monster classes are {monster_classes}')


for target_class, abil_filenames in (ability_files.items()):
    found_class_abils = {}
    # load abilities
    for abil in abil_filenames:
        abil_path = f'{classes_dir}/{target_class}/abilities/{abil}'
        with open(abil_path) as file:
            loaded_data = yaml.load(file.read(), Loader=Loader)
            found_class_abils[loaded_data.get('name')] = loaded_data
    loaded_abilities[target_class] = found_class_abils


class ClassStats(object):
    def __init__(self, d):
        self.__dict__ = d


class ClassAbilities(object):
    def __init__(self, d):
        self.__dict__ = d


class ClassConfig(object):
    def __init__(self, d):
        self.__dict__ = d


class_abilities = ClassAbilities(loaded_abilities)
class_stats = ClassStats(loaded_class_stats)
class_config = ClassConfig(loaded_class_config)
